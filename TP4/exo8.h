#ifndef exo8_h
#define exo8_h
#include <stdbool.h>

void remplirTab(int* tab, int n);

bool recherche(int* tab, int n, int val);

float moyenne(int* tab, int n);

bool palindrome(int* tab, int n);

void minmax(int* tab, int n, int* min, int* max);

#endif

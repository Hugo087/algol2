#include "exo9.h"

void chercheChaine(char** tabString, int n, char caractere){
  int i, k;
  for(i = 0; i < n; i++){
    k = 0;
    while(*(*(tabString + i) + k) != '\0'){
      if(*(*(tabString + i) + k) == caractere){
        printf("%s\n",*(tabString + i));
        break;
      }
      k++;
    }
  }
}

int plusLongueChaine(char** tabString, int n){
  int i, k, res = 0, indice;
  for(i = 0; i < n; i++){
    k = 0;
    while(*(*(tabString + i) + k) != '\0'){
      k++;
    }
    if(k > res){
      res = k;
      indice = i;
    }
  }
  return indice;
}

int tailleChaine(char* chaine){
  int i = 0;
  while(*(chaine + i) != '\0'){
    i++;
  }
  return i + 1;
}

char* concat(char** tabString, int n){
  int i, k,c =0, tailleTotal = 0;
  for(i = 0; i < n ; i++){
    tailleTotal += tailleChaine(*(tabString + i ));
  }
  char* res = (char*)malloc(sizeof(char)* tailleTotal);
  for(i = 0; i < n; i++){
    k = 0;
    while(*(*(tabString + i) + k) != '\0'){
      *(res + c) = *(*(tabString + i) + k);
      c++;
      k++;
    }
  }
  return res;
}

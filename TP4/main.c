#include "exo8.h"
#include "exo9.h"
#include <stdio.h>
#include <stdlib.h>

int main(){

  // Exo 8
  printf("Exo 8 : \n");
  int* tab = (int*)malloc(sizeof(int)*100);
  remplirTab(tab, 100);
  printf("Recherche valeur dans tab : \n");
  bool test = recherche(tab, 100, 99);
  if(test == 1){
    printf("True\n");
  }
  else{
    printf("False\n");
  }
  printf("%f\n", moyenne(tab, 100));

  int min;
  int max;

  minmax(tab, 100, &min, &max);

  printf("max = %d\n",max);
  printf("min = %d\n",min);

  int* tab2 = (int*)malloc(sizeof(int)*5);
  *tab2 = 100;
  *(tab2 + 1) = 5;
  *(tab2 + 2) = 76;
  *(tab2 + 3) = 5;
  *(tab2 + 4) = 100;

  printf("Palindrome tab 1 : \n");
  test = palindrome(tab, 100);
  if(test == 1){
    printf("True\n");
  }
  else{
    printf("False\n");
  }

  printf("Palindrome tab 2 : \n");
  test = palindrome(tab2, 5);
  if(test == 1){
    printf("True\n");
  }
  else{
    printf("False\n");
  }

  printf("Exo 9 : \n");
  int i;
  char** c = (char**)malloc(sizeof(char*) * 5);
  for(i = 0; i < 5; i++){
    *(c +i) = (char*)malloc(sizeof(char) * 256);
  }
  for(i = 0; i < 5; i++){
    scanf("%s", *(c+i));
  }

  chercheChaine(c, 5, 'n');

  printf("%d\n",plusLongueChaine(c, 5));

  printf("%s\n",concat(c, 5));

}

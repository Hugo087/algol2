#ifndef exo9_h
#define exo9_h
#include <stdio.h>
#include <stdlib.h>

void chercheChaine(char** tabString, int n, char caractere);

int plusLongueChaine(char** tabString, int n);

int tailleChaine(char* chaine);

char* concat(char** tabString, int n);

#endif

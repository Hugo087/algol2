#include "exo8.h"

void remplirTab(int* tab, int n){
  int i;
  for(i = 0; i < n ; i++){
    *(tab + i) = i;
  }
}

bool recherche(int* tab, int n, int val){
  int i;
  for(i = 0; i < n ; i++){
    if(*(tab + i) == val){
      return true;
    }
  }
  return false;
}

float moyenne(int* tab, int n){
  int i, j;
  for(i = 0; i < n ; i ++){
    j += *(tab + i);
  }
  return (j/n);
}

bool palindrome(int* tab, int n){
  int i;
  for(i = 0; i < n ; i ++){
    if(*(tab + i) != *(tab + (n - i - 1))){
      return false;
    }
  }
  return true;
}

void minmax(int* tab, int n, int* min, int* max){
  int i;
  *min = *tab;
  *max = *tab;
  for(i = 1; i < n ; i ++){
    if(*(tab + i) < *min){
      *min = *(tab +i);
    }
    if(*(tab + i) > *max){
      *max = *(tab +i);
    }
  }
}

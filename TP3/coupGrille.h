#ifndef coupGrille_h
#define coupGrille_h
#include <stdio.h>
#include <stdbool.h>

typedef struct{
  int x;
  int y;
} Coup;

typedef int Grille[6][7];

void initGrille(Grille grille);

void afficherGrille(Grille grille);

void miseAJourGrille(Grille grille, Coup coup, int joueur);

void faireCoup(Grille grille, int joueur);

bool verifGagnerLigne(Grille grille, int joueur);

bool verifGagnerColonne(Grille grille, int joueur);

bool verifGagnerDiagonal(Grille grille, int joueur);

bool verifGagner(Grille grille, int joueur);

void lancerPartie(int nbjoueur);

#endif

#include "coupGrille.h"



void initGrille(Grille grille){
  int i, j;
  for(i = 0; i < 6; i++){
    for(j = 0; j < 7; j++){
      grille[i][j] = 0;
    }
  }
}

void afficherGrille(Grille grille){
  int i, j;
  for(i = 0; i < 6; i++){
    for(j = 0; j < 7; j++){
      printf("%d ", grille[i][j]);
    }
    printf("\n");
  }
}

void miseAJourGrille(Grille grille, Coup coup, int joueur){
  while(grille[coup.y][coup.x] == 0 && coup.y <= 5){
    coup.y++;
  }
  grille[--coup.y][coup.x] = joueur;
}

void faireCoup(Grille grille, int joueur){
  Coup coup;
  do
  {
    printf("Coup en x : ");
    scanf("%d", &coup.x);
    printf("Coup en y : ");
    scanf("%d", &coup.y);
    if(coup.x < 0 || coup.y < 0 || coup.x > 7 || coup.y > 6 || grille[coup.y][coup.x] != 0)
      printf("Les valeurs sont incohérentes, veuillez les resaisir : \n" );
  }
  while(coup.x < 0 || coup.y < 0 || coup.x > 7 || coup.y > 6 || grille[coup.y][coup.x] != 0 );
  miseAJourGrille(grille, coup, joueur);
}

bool verifGagnerLigne(Grille grille, int joueur){
  int i, j, k;
  for(i = 0; i < 6 ; i++){
    k = 0;
    for(j = 0; j < 7; j++){
      if(grille[i][j] == joueur){
        k++;
        if(k >= 4){
          return true;
        }
      }
    }
  }
  return false;
}

bool verifGagnerColonne(Grille grille, int joueur){
  int i, j, k;
  for(i = 0; i < 7; i++){
    k = 0;
    for(j = 0; j < 6; j++){
      if(grille[j][i] == joueur){
        k++;
        if(k >= 4){
          return true;
        }
      }
    }
  }
  return false;
}

int verifDiagonalDroite(Grille grille, int y, int x,int joueur){
  if(grille[y][x] == joueur && y >= 0 && x >= 0 && y < 6 && x < 7){
    return (verifDiagonalDroite(grille, y-1,x+1, joueur) + 1);
  }
  return 0;
}

int verifDiagonalGauche(Grille grille, int y, int x,int joueur){
  if(grille[y][x] == joueur && y >= 0 && x >= 0 && y < 6 && x < 7){
    return (verifDiagonalGauche(grille, y-1,x-1, joueur) + 1);
  }
  return 0;
}

bool verifGagnerDiagonal(Grille grille, int joueur){
  int i, j, res = 0;
  for(i = 5; i >= 0 ; i--){
    for(j = 0; j < 7; j++){
      res = verifDiagonalDroite(grille, i, j, joueur);
      if(res == 4){
        return true;
      }
      res = verifDiagonalGauche(grille, i, j, joueur);
      if(res == 4){
        return true;
      }
    }
  }
  return false;
}

bool verifGagner(Grille grille, int joueur){
  if(verifGagnerLigne(grille, joueur) || verifGagnerColonne(grille, joueur) || verifGagnerDiagonal(grille, joueur)){
      return true;
  }
  return false;
}

void lancerPartie(int nbjoueur){
  int i = nbjoueur, k = 0;
  Grille grille;
  initGrille(grille);
  afficherGrille(grille);
  do{
    if(i == nbjoueur)
      i = 1;
    else
      i++;
    printf("Joueur %d\n", i);
    faireCoup(grille, i);
    afficherGrille(grille);
  }while(!verifGagner(grille, i) && k <= (6*7));
  if(k >= (6*7)){
    printf("Egalité !\n" );
  }
  else{
    printf("Joueur %d à gagner ! \n",i);
  }
}

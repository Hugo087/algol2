#ifndef exo15_h
#define exo15_h
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    int id;
    char mm[100];
    int an;
    float cout;
    int km;
} Voiture;

typedef struct Cell
{
    Voiture voiture;
    struct Cell *s;
} Cellule;

Voiture saisieVoiture();

Cellule *creationListe();

void afficherVoiture(Voiture voiture);

void afficherListe(Cellule *liste);

Voiture rechercheVoiture(Cellule *liste, int id);

void ajouterVoiture(Cellule *liste, Voiture v, int id);

void libererMemoire(Cellule *liste);

#endif

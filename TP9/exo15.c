#include "exo15.h"

Voiture saisieVoiture()
{
    Voiture voiture;
    printf("Id de la voiture : ");
    scanf("%d", &voiture.id);
    printf("Modele/marque de la voiture : ");
    scanf("%s", voiture.mm);
    printf("Annee de la voiture : ");
    scanf("%d", &voiture.an);
    printf("Cout de la voiture : ");
    scanf("%f", &voiture.cout);
    printf("Kilometrage de la voiture : ");
    scanf("%d", &voiture.km);
    return voiture;
}

Cellule *creationListe()
{
    Cellule *liste = (Cellule *)malloc(sizeof(Cellule));
    liste->s = NULL;
    liste->voiture = saisieVoiture();
    return liste;
}

void afficherVoiture(Voiture voiture)
{
    printf("Id de la voiture : %d\n", voiture.id);
    printf("Modele/marque de la voiture : %s\n", voiture.mm);
    printf("Annee de la voiture : %d\n", voiture.an);
    printf("Cout de la voiture : %f\n", voiture.cout);
    printf("Kilometrage de la voiture : %d\n", voiture.km);
}

void afficherListe(Cellule *liste)
{
    if (liste != NULL)
    {
        afficherVoiture(liste->voiture);
        afficherListe(liste->s);
    }
}

Voiture rechercheVoiture(Cellule *liste, int id)
{
    Voiture v;
    v.id = -1;
    if (liste != NULL)
    {
        if (liste->voiture.id > id)
        {
            return liste->voiture;
        }
        return rechercheVoiture(liste->s, id);
    }
    return v;
}

void ajouterVoiture(Cellule *liste, Voiture v, int id)
{
    Cellule *temp;
    if (liste != NULL)
    {
        if (liste->voiture.id == id)
        {
            temp = (Cellule *)malloc(sizeof(Cellule));
            temp->s = liste->s;
            liste->s = temp;
            temp->voiture = v;
        }
        else
            ajouterVoiture(liste->s, v, id);
    }
}

void libererMemoire(Cellule *liste)
{
    if (liste != NULL)
    {
        libererMemoire(liste->s);
        free(liste);
    }
}
#include "exo15.h"
#include "exo17.h"

int main(){
  

  Cellule* liste = creationListe();
  ajouterVoiture(liste, saisieVoiture(), 1);
  ajouterVoiture(liste, saisieVoiture(), 2);
  ajouterVoiture(liste, saisieVoiture(), 3);
  ajouterVoiture(liste, saisieVoiture(), 4);

  afficherListe(liste);

  afficherVoiture(rechercheVoiture(liste, 2));

  libererMemoire(liste);

  return 1;
}

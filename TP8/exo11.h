#ifndef exo9_h
#define exo9_h
#include <stdio.h>
#include <stdlib.h>

typedef struct Cell{
  float val;
  struct Cell* s;
}Cellule;


int tailleListe(Cellule *cell);

float valAtPos(Cellule *cell, int pos);

int nbOccurence(Cellule *cell, float r);

Cellule* suprTete(Cellule *cell);

void suprQueue(Cellule *cell);

Cellule *addTete(Cellule *cell, float val);

Cellule *fusionListe(Cellule *cell1, Cellule *cell2);

Cellule *getElementAtPos(Cellule *cell, int pos);

void supprimerElement(Cellule *cell, int pos);

void supprimerDoublon(Cellule *cell);

void positive_negative(Cellule *listeReel, Cellule *listePositive, Cellule *listeNegative);

void afficherListe(Cellule *cell);

#endif

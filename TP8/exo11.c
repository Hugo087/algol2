#include "exo11.h"

int tailleListe(Cellule *cell)
{
    if (cell == NULL)
        return 0;
    if (cell->s == NULL)
        return 1;
    return tailleListe(cell->s) + 1;
}

float valAtPos(Cellule *cell, int pos)
{
    if (cell == NULL)
        return 0;
    if (pos == 0)
        return cell->val;
    if (cell->s == NULL)
        return 0;
    return valAtPos(cell->s, pos - 1);
}

int nbOccurence(Cellule *cell, float r)
{
    int occ = 0;
    if (cell == NULL)
        return 0;
    while (cell->s != NULL)
    {
        if (r == cell->val)
        {
            occ++;
        }
        cell = cell->s;
    }
    return occ;
}

Cellule* suprTete(Cellule *cell)
{
    Cellule *temp;
    if (cell != NULL)
    {
        temp = cell->s;
        free(cell);
    }
    return temp;
}

void suprQueue(Cellule *cell)
{
    if (cell != NULL)
    {
        if (cell->s == NULL)
        {
            cell = NULL;
        }
        else
        {
            while (cell->s->s != NULL)
            {
                cell = cell->s;
            }
            free(cell->s);
            cell->s = NULL;
        }
    }
}

Cellule* addTete(Cellule *cell, float val)
{
    Cellule *nouveau;
    nouveau = (Cellule *)malloc(sizeof(Cellule));
    nouveau->s = cell;
    nouveau->val = val;
    return nouveau;
}

Cellule *fusionListe(Cellule *cell1, Cellule *cell2)
{
    Cellule *cell;
    cell = (Cellule *)malloc(sizeof(Cellule) * tailleListe(cell1) + sizeof(Cellule) * tailleListe(cell2));
    while (cell1 != NULL && cell2 != NULL)
    {
        if (cell1->val >= cell2->val)
        {
            cell = addTete(cell, cell1->val);
            cell1 = cell1->s;
        }
        else
        {
            cell = addTete(cell, cell2->val);
            cell2 = cell2->s;
        }
    }
    if (cell1 == NULL)
    {
        while (cell2 != NULL)
        {
            cell = addTete(cell, cell2->val);
            cell2 = cell2->s;
        }
    }
    else
    {
        while (cell1 != NULL)
        {
            cell = addTete(cell, cell1->val);
            cell1 = cell1->s;
        }
    }
    return cell;
}

Cellule *getElementAtPos(Cellule *cell, int pos)
{
    if (cell != NULL)
    {
        while (pos != 0 && cell->s != NULL)
        {
            pos--;
            cell = cell->s;
        }
        if (pos == 0)
        {
            return cell;
        }
    }
    return NULL;
}

void supprimerElement(Cellule *cell, int pos)
{
    Cellule *elem = getElementAtPos(cell, pos);
    Cellule *prec = getElementAtPos(cell, pos - 1);
    if (elem != NULL)
    {
        prec->s = elem->s;
        free(elem);
    }
}

void supprimerDoublon(Cellule *cell)
{   int i = 0;
    Cellule *temp = cell;
    if (cell != NULL)
    {
        while (temp->s != NULL)
        {
            if(temp->val == temp->s->val){
               supprimerElement(cell, i); 
            }
            else{
                temp = temp->s;
                i++;
            }
        }
    }
}

void positive_negative(Cellule *listeReel, Cellule *listePositive, Cellule *listeNegative){
    int posi = 0, nega = 1;
    Cellule* temp = listeReel;
    if(listeReel == NULL){
        listePositive = NULL;
        listeNegative = NULL;
    }
    else{
        while(temp != NULL){
            if(temp->val >= 0)
                posi++;
            else
                nega++;
            temp = temp->s;
        }
        listePositive = (Cellule*)malloc(sizeof(Cellule) * posi);
        listeNegative = (Cellule*)malloc(sizeof(Cellule) * nega);
        while(listeReel != NULL){
            if(listeReel->val >= 0){
                listePositive->val = listeReel->val;
                listeReel = listeReel->s;
            }
            else{
                listeNegative->val = listeReel->val;
                listeReel = listeReel->s;
            }

        }
    }
}

void afficherListe(Cellule *cell){
    if(cell == NULL)
        printf("NULL\n");
    while(cell->s != NULL){
        printf("%f, ",cell->val);
        cell = cell->s;
    }
    printf("\n");
}
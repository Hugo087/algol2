float calcul(float a, float b, char operation){
  switch(operation){
    case '+':
      return a + b;
    case '-':
      return a - b;
    case '*':
      return a * b;
    case '/':
      if(b != 0){
        return a / b;
      }
      else{
        return 0;
      }
    default :
      return 0;
  }
}

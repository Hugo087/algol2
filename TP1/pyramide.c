#include <stdio.h>

void pyramide(){
  int hauteur, i, j, k;
  scanf("%d", &hauteur);
  for(i = hauteur; i != 0; i--){
    for(k = 0; k < (((hauteur-1) * 2 + 1) - ((i-1) * 2 + 1))/2; k++){
      printf(" ");
    }
      for(j = 0; j < ((i-1) * 2 + 1); j++){
        printf("*");
      }
      printf("\n");
    }
  }

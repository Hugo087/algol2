#include <stdio.h>

void table(int a, int b){
  int i, j;
  if(a > b){
    for(i = b ; i <= a ; i++){
      printf("Table de multiplication de %d :\n",i);
      for(j = 1; j <= 10; j++){
        printf("%d * %d = %d\n",i , j , i * j);
      }
    }
  }
  else{
    for(i = a ; i <= b ; i++){
      printf("Table de multiplication de %d :\n",i);
      for(j = 1; j <= 10; j++){
        printf("%d * %d = %d\n",i , j , i * j);
      }
    }
  }
}

#include <stdio.h>

int nbVal(){
  int n = 0;
  while(n < 1 || n > 20){
    printf("Nombre de valeurs pour la tableau (Entre 1 et 20) : ");
    scanf("%d", &n);
  }
  return n;
}

void saisirTab(int* t, int n){
  int i;
  for(i = 0; i < n; i++)
  {
    printf("Valeurs %d :",i );
    scanf("%d", &t[i]);
  }
}

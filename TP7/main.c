#include "exo7.h"
#include "exo9.h"

int main(){
  /* Exo 7 */
  /* numero 1 a 6 */
  int n;
  printf("Nombre de vehicule : ");
  scanf("%d", &n);

  Vehicule* tabVehicule = alloueVehicule(n);
  saisieVehicule(tabVehicule, n);
  afficherVehicule(tabVehicule, n, 2016);
  Vehicule a = plusCherVehicule(tabVehicule, n);
  afficherVehicule(&a, 1, a.annee);
  free(tabVehicule);

/* numero 7 a 10 */
  Store s;
  printf("Nombre de vehicule : ");
  scanf("%d", &n);

  saisieStore(&s, n);
  afficherStore(&s);
  triStore(&s);
  afficherStore(&s);
  free(s.vehicules);

  /* Exo 9 */
  printf("Nombre de donnees : ");
  scanf("%d", &n);
  Data* tabdata = saisieData(n);
  bool t = checkJour(tabdata, n, Lundi);
  if(t == false)
    printf("False\n");
  else
    printf("True\n");
  free(tabdata);

  return 1;
}

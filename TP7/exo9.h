#ifndef exo9_h
#define exo9_h
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

enum Jour { Lundi, Mardi, Mercredi, Jeudi, Vendredi, Samedi, Dimanche};

typedef struct{
  float temp;
  enum Jour jour;
}Data;

float* saisieTemperature(int n);

void afficherTemperature(float* tabTemp, int n);

Data* saisieData(int n);

bool checkJour(Data* tabData, int n, enum Jour jour);

#endif

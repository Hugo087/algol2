#include "exo9.h"

float* saisieTemperature(int n){
  int i;
  float* tabTemp = (float*)malloc(sizeof(float)*n);
  for(i = 0; i < n ; i++){
    printf("Temperature numero %d : ", i);
    scanf("%f",&tabTemp[i]);
  }
  return tabTemp;
}

void afficherTemperature(float* tabTemp, int n){
  printf("Temperature %f", tabTemp[n-1]);
  if(n > 1){
    afficherTemperature(tabTemp, n-1);
  }
}

Data* saisieData(int n){
  int i;
  Data* tabData = (Data*)malloc(sizeof(Data)*n);
    for(i = 0; i < n ; i++){
      printf("Temperature numero %d : ", i);
      scanf("%f", &tabData[i].temp);
      printf("Jour de la semaine : ");
      scanf("%d", (int*)&tabData[i].jour);
    }
  return tabData;
}

bool checkJour(Data* tabData, int n,enum Jour jour){
  if(tabData[n-1].jour == jour){
    return true;
  }
  if(n == 1){
    return false;
  }
  return checkJour(tabData, n-1, jour);
}

#ifndef exo7_h
#define exo7_h
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct{
  char type[4];
  float prix;
  int kilometrage;
  int annee;
}Vehicule;

typedef struct{
  Vehicule* vehicules;
  int nbVehicules;
}Store;

Vehicule* alloueVehicule(int n);

void saisieVehicule(Vehicule* tab, int n);

void afficherVehicule(Vehicule* tab, int n, int annee);

Vehicule plusCherVehicule(Vehicule* tab, int n);

void saisieStore(Store* s, int n);

void triStore(Store* s);

void afficherStore(Store* s);

#endif

#include "exo7.h"

Vehicule* alloueVehicule(int n){
  Vehicule* tab = (Vehicule*)malloc(sizeof(Vehicule)*n);
  return tab;
}

void saisieVehicule(Vehicule* tab, int n){
  if(n > 1){
    saisieVehicule(tab, n-1);
  }
  printf("Type du vehicule : ");
  scanf("%s", tab[n-1].type);
  printf("Prix du vehicule : ");
  scanf("%f", &tab[n-1].prix);
  printf("Kilometrage du vehicule : ");
  scanf("%d", &tab[n-1].kilometrage);
  printf("Annee du vehicule : ");
  scanf("%d", &tab[n-1].annee);
}

void afficherVehicule(Vehicule* tab, int n, int annee){
  if(n > 1){
    afficherVehicule(tab, n-1, annee);
  }
  if(annee == tab[n-1].annee){
    printf("Type du vehicule : %s\n", tab[n-1].type);
    printf("Prix du vehicule : %f\n", tab[n-1].prix);
    printf("Kilometrage du vehicule : %d\n", tab[n-1].kilometrage);
    printf("Annee du vehicule : %d\n", tab[n-1].annee);
  }
}

Vehicule plusCherVehicule(Vehicule* tab, int n){
  int i, prix = 0, index;
  for(i = 0; i < n ; i++){
    if(tab[i].prix > prix){
      prix = tab[n].prix;
      index = i;
    }
  }
  return tab[index];
}

void saisieStore(Store* s, int n){
  s->nbVehicules = n;
  s->vehicules = alloueVehicule(n);
  saisieVehicule(s->vehicules, s->nbVehicules);
}

void triStore(Store* s){
    int i, j;
    Vehicule buffer;
    for(i = 1; i < s->nbVehicules; i++){
      buffer = s->vehicules[i];
      for(j = i; 0 < j && s->vehicules[j - 1].kilometrage > buffer.kilometrage; j--){
        s->vehicules[j] = s->vehicules[j-1];
      }
      s->vehicules[j] = buffer;
    }
}

void afficherStore(Store* s){
  int i;
  for(i = 0; i < s->nbVehicules; i++){
    printf("Vehicule numero %d :\n", i);
    printf("Type du vehicule : %s\n", s->vehicules[i].type);
    printf("Prix du vehicule : %f\n", s->vehicules[i].prix);
    printf("Kilometrage du vehicule : %d\n", s->vehicules[i].kilometrage);
    printf("Annee du vehicule : %d\n", s->vehicules[i].annee);
  }
}

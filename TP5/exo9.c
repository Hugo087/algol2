#include "exo9.h"

void base10to2(int number){
    if(number == 0 ){
      printf("0");
    }
    else if(number == 1){
      printf("1");
    }
    else if(number % 2 == 1){
      base10to2(number/2);
      printf("1");
    }
    else if(number % 2 == 0){
      base10to2(number/2);
      printf("0");
    }
}

int powe(int a, int pui){
  int i, res = 1;
  for(i = 0; i < pui; i++){
    res = res * a;
  }
  return res;
}

int base2(int number, int puis){
  if(number == 0){
    return 0;
  }
  else if(number == 1){
    return powe(2,puis);
  }
  else if(number % 10 == 0){
    return base2(number / 10, puis +1);
  }
  else if(number % 10 == 1){
    return powe(2, puis) + base2(number / 10, puis +1);
  }
  return 0;
}

void base2to10(int number){
  printf("%d\n", base2(number, 0));
}

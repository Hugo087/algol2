#include "exo11.h"

void saisie(int* tab, int nbval){
  if(nbval == 1){
    scanf("%d\n", &tab[nbval-1]);
  }
  else{
    saisie(tab, nbval-1);
    scanf("%d\n", &tab[nbval-1]);
  }
}

void affichage(int* tab, int nbval){
  if(nbval == 1){
    printf("%d", tab[nbval-1]);
  }
  else{
    affichage(tab, nbval-1);
    printf("%d", tab[nbval-1]);
  }
}

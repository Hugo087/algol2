#ifndef exo10_h
#define exo10_h
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int longueurChaine(char* chaine);

void chainelenver(char* chaine, int taille);

bool egalChaine(char* chaine1, int l1,char * chaine2, int l2);

bool estDansChaine(char* chaine, int l, char cara);

#endif

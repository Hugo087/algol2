#ifndef tab2dim_h
#define tab2dim_h

void saisieTab(int n, int m, float** tab);

void afficherTabLigne(int n, int m, float** tab);

void afficherTabColonne(int n, int m, float** tab);

void sommeColonne(int n, int m, float** tab, float* tab2);

void sommeLigne(int n, int m, float** tab, float* tab2);

void afficherTab(int a, float* tab);

int ajouterColonne(int n, int m, float** tab);

void trierLigne(int n, int m, float** tab,float** tabf);

#endif

#include <stdio.h>
#include <stdlib.h>

void saisieTab(int n, int m, float** tab){
  int i, j;
  for(i = 0; i < n; i++){
    printf("Ligne %d :\n", i);
    for(j = 0; j < m; j++){
      scanf("%f", &tab[i][j]);
    }
  }
}

void afficherTabColonne(int n, int m, float** tab){
  int i, j;
  printf("Affichage par colonne :\n");
  for(i = 0; i < n; i++){
    for(j = 0; j < m; j++){
      printf("%f ", tab[i][j]);
    }
    printf("\n");
  }
}

void afficherTabLigne(int n, int m, float** tab){
  int i, j;
  printf("Affichage par ligne :\n");
  for(i = 0; i < m; i++){
    for(j = 0; j < n; j++){
      printf("%f ", tab[j][i]);
    }
    printf("\n");
  }
}

void sommeLigne(int n, int m, float** tab, float* tab2 ){
  int i, j;
  for(i = 0; i < n; i++){
    for(j = 0; j < m; j++){
      tab2[i] += tab[i][j];
    }
  }
}

void sommeColonne(int n, int m, float** tab, float* tab2){
  int i, j;
  for(i = 0; i < m; i++){
    for(j = 0; j < n; j++){
      tab2[i] += tab[j][i];
    }
  }
}

void afficherTab(int a, float* tab){
  int i;
  for(i = 0; i < a; i++){
    printf("%f ",tab[i] );
  }
}

int ajouterColonne(int n, int m, float** tab){
  int i;
  if(m <= 5){
    for(i = 0; i < n; i++){
      scanf("%f",&tab[i][m]);
    }
    return (m+1);
  }
  return -1;
}

void trierLigne(int n, int m, float** tab,float** tabf){
  int i, k = 0, k1 = 0, j;
  float *tab2 = (float*)malloc(n*sizeof(float));
  sommeLigne(n, m, tab, tab2);
  for(j = 0; j < n; j++){
    for(i = 0; i < n ; i++){
      if(tab2[i] > k){
        k = tab2[i];
        k1 = i;
      }
    }
    for(i = 0; i < m; i++){
      tabf[j][i] = tab[k1][i];
    }
    tab2[k1] = 0;
    k = 0;
}

}

#ifndef exo1_h
#define exo1_h
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
  char lieu[15];
  int places;
  float tarif;
}Evenement;

Evenement saisieEvenement();

void afficherEvenement(Evenement event);

void modifierTarif(Evenement* event);

void ajouterEvenement(Evenement* event, int indice);

void afficherTabEvenement(Evenement* event, int nbVal);

float plusGrandIndice(Evenement* event, int nbVal);

#endif

#include "exo3.h"

int getChiffre(int entier, int index){
  if(index == 0){
    return entier%10;
  }
  return getChiffre(entier /10, index-1)%10;
}

int tailleEntier(int entier){
  if(entier == 0){
    return 0;
  }
  return tailleEntier(entier /10) +1;
}

int calculFactoriel(int entier){
  int i, val=1;
  for(i = 1; i <= entier; i++){
    val = val * i;
  }
  return val;
}

bool estFactorion(int entier){
  int temp = 0;
  if(entier < 0){
    return false;
  }
  int i;
  for(i = 0; i < tailleEntier(entier); i++){
    temp += calculFactoriel(getChiffre(entier, i));
  }
  if(temp == entier){
    return true;
  }
  return false;
}

void afficherFactorion(int a, int b){
  int i;
  for(i = a; i < b; i++){
    if(estFactorion(i)){
      printf("%d est factorion\n",i);
    }
  }
}

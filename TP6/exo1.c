#include "exo1.h"

Evenement saisieEvenement(){
  Evenement event;
  printf("Nom du lieu : ");
  scanf("%s", event.lieu);
  printf("Nombre de places : ");
  scanf("%d", &event.places);
  printf("Tarif de l'evenement : ");
  scanf("%f", &event.tarif);
  return event;
}

void afficherEvenement(Evenement event){
  printf("Nom du lieu : %s\n", event.lieu);
  printf("Nombre de places : %d\n", event.places);
  printf("Tarif de l'evenement : %f\n", event.tarif);
}

void modifierTarif(Evenement* event){
  printf("Saissez le nouveau tarif : ");
  scanf("%f", &event->tarif);
}

void ajouterEvenement(Evenement* event, int indice){
  event[indice] = saisieEvenement();
}

void afficherTabEvenement(Evenement* event, int nbVal){
  int i;
  for(i = 0; i < nbVal; i++){
    afficherEvenement(event[i]);
  }
}

float plusGrandIndice(Evenement* event, int nbVal){
  if(nbVal == 1){
    return event[0].tarif;
  }
  float autre = plusGrandIndice(event, nbVal-1);
  if(event[nbVal-1].tarif > autre){
    return event[nbVal-1].tarif;
  }
  else{
    return autre;
  }
}
